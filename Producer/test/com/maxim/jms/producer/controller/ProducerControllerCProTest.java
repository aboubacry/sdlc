package com.maxim.jms.producer.controller;

import junit.framework.TestCase;
import junit.framework.TestSuite;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.web.servlet.ModelAndView;

import com.maxim.jms.producer.model.Vendor;

/**
 * The class <code>ProducerControllerCProTest</code> contains tests for the class <code>{@link ProducerController}</code>.
 *
 * @generatedBy CodePro at 2/24/15 10:31 AM, using the Spring generator
 * @author abouba
 * @version $Revision: 1.0 $
 */
public class ProducerControllerCProTest extends TestCase {
	private ApplicationContext context;

	/**
	 * Run the ProducerController() constructor test.
	 *
	 * @generatedBy CodePro at 2/24/15 10:31 AM
	 */
	public void testProducerController_1()
		throws Exception {
		ProducerController result = new ProducerController();
		assertNotNull(result);
		// add additional test code here
	}

	/**
	 * Run the ModelAndView processRequest(Vendor,Model) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 2/24/15 10:31 AM
	 */
	public void testProcessRequest_1()
		throws Exception {
		ProducerController fixture = new ProducerController();
		Vendor vendor = new Vendor();
		vendor.setVendorName("UPSSSS");
		vendor.setFirstName("Bobbbb");
		vendor.setLastName("Thomassss");
		vendor.setAddress("123 Main sssst");
		vendor.setCity("maintownnnn");
		vendor.setState("iowa");
		vendor.setZipCode("12345555");
		vendor.setEmail("bob@upssss.com");
		vendor.setPhoneNumber("123-456-788880");
		ExtendedModelMap model = new ExtendedModelMap();
		
		context = new ClassPathXmlApplicationContext("spring/application-config.xml");
		fixture = (ProducerController) context.getBean("producerController");
		

		ModelAndView result = fixture.processRequest(vendor, model);

		// add additional test code here
		// An unexpected exception was thrown in user code while executing this test:
		//    java.lang.NullPointerException
		//       at com.maxim.jms.producer.controller.ProducerController.processRequest(ProducerController.java:35)
		assertNotNull(result);
		assertEquals("index", result.getViewName());

	}

	/**
	 * Run the String renderVendorPage(Vendor,Model) method test.
	 *
	 * @throws Exception
	 *
	 * @generatedBy CodePro at 2/24/15 10:31 AM
	 */
	public void testRenderVendorPage_1()
		throws Exception {
		ProducerController fixture = new ProducerController();
		Vendor vendor = new Vendor();
		ExtendedModelMap model = new ExtendedModelMap();

		String result = fixture.renderVendorPage(vendor, model);

		// add additional test code here
		assertEquals("index", result);
		assertEquals(0, model.size());
	}

	/**
	 * Perform pre-test initialization.
	 *
	 * @throws Exception
	 *         if the initialization fails for some reason
	 *
	 * @see TestCase#setUp()
	 *
	 * @generatedBy CodePro at 2/24/15 10:31 AM
	 */
	protected void setUp()
		throws Exception {
		super.setUp();
		// add additional set up code here
	}

	/**
	 * Perform post-test clean-up.
	 *
	 * @throws Exception
	 *         if the clean-up fails for some reason
	 *
	 * @see TestCase#tearDown()
	 *
	 * @generatedBy CodePro at 2/24/15 10:31 AM
	 */
	protected void tearDown()
		throws Exception {
		super.tearDown();
		// Add additional tear down code here
	}

	/**
	 * Launch the test.
	 *
	 * @param args the command line arguments
	 *
	 * @generatedBy CodePro at 2/24/15 10:31 AM
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			// Run all of the tests
			junit.textui.TestRunner.run(ProducerControllerCProTest.class);
		} else {
			// Run only the named tests
			TestSuite suite = new TestSuite("Selected tests");
			for (int i = 0; i < args.length; i++) {
				TestCase test = new ProducerControllerCProTest();
				test.setName(args[i]);
				suite.addTest(test);
			}
			junit.textui.TestRunner.run(suite);
		}
	}
}