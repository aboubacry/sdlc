package com.maxim.jms.listener;

import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.replay;
import static org.easymock.EasyMock.verify;
import static org.junit.Assert.assertNotNull;

import javax.jms.TextMessage;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * The class <code>ConsumerListenerCProTest</code> contains tests for the class
 * <code>{@link ConsumerListener}</code>.
 * 
 * @generatedBy CodePro at 2/23/15 6:32 PM
 * @author abouba
 * @version $Revision: 1.0 $
 */
public class ConsumerListenerCProTest {

	private TextMessage message;
	private ApplicationContext context;
	private ConsumerListener listener;
	private String json = "{vendorName:\"Microsofttes3t\",firstName:\"BobTest3\",lastName:\"SmithTest3\",address:\"123 Main test3\",city:\"TulsaTest3\",state:\"OKTest3\",zip:\"71345Test3\",email:\"Bob@microsoft.test3\",phoneNumber:\"test-123-3333\"}";

	/**
	 * Perform pre-test initialization.
	 * 
	 * @throws Exception
	 *             if the initialization fails for some reason
	 * 
	 * @generatedBy CodePro at 2/23/15 6:32 PM
	 */
	@Before
	public void setUp() throws Exception {
		context = new ClassPathXmlApplicationContext(
				"/spring/application-config.xml");
		listener = (ConsumerListener) context.getBean("consumerListener");
		message = createMock(TextMessage.class);
	}

	/**
	 * Perform post-test clean-up.
	 * 
	 * @throws Exception
	 *             if the clean-up fails for some reason
	 * 
	 * @generatedBy CodePro at 2/23/15 6:32 PM
	 */
	@After
	public void tearDown() throws Exception {
		((ConfigurableApplicationContext) context).close();
	}

	@Test
	public void testOnMessage_1() throws Exception {
		expect(message.getText()).andReturn(json);
		replay(message);
		listener.onMessage(message);
		verify(message);
	}

	/**
	 * Run the ConsumerListener() constructor test.
	 * 
	 * @generatedBy CodePro at 2/23/15 6:32 PM
	 */
	@Test
	public void testConsumerListener_1() throws Exception {
		ConsumerListener result = new ConsumerListener();
		assertNotNull(result);
	}

	/**
	 * Run the void onMessage(Message) method test.
	 * 
	 * @throws Exception
	 * 
	 * @generatedBy CodePro at 2/23/15 6:32 PM
	 */
	@Test
	public void testOnMessage_2() throws Exception {
		/*ConsumerListener fixture = new ConsumerListener();
		fixture.jmsTemplate = new JmsTemplate();
		fixture.consumerAdapter = new ConsumerAdapter();
		Message message = new ActiveMQTextMessage();

		fixture.onMessage(message);*/

		expect(message.getText()).andReturn(json);
		replay(message);
		listener.onMessage(message);
		verify(message);
	
	}

	/**
	 * Launch the test.
	 * 
	 * @param args
	 *            the command line arguments
	 * 
	 * @generatedBy CodePro at 2/23/15 6:32 PM
	 */
	public static void main(String[] args) {
		new org.junit.runner.JUnitCore().run(ConsumerListenerCProTest.class);
	}
}